#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: client
#------------------------------------------------------------

CREATE TABLE client(
                       id  Int  Auto_increment  NOT NULL ,
                       nom Varchar (50) NOT NULL
    ,CONSTRAINT client_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: candyBox
#------------------------------------------------------------

CREATE TABLE candyBox(
                         id Int  Auto_increment  NOT NULL
    ,CONSTRAINT candyBox_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: couleur
#------------------------------------------------------------

CREATE TABLE couleur(
                        id  Int  Auto_increment  NOT NULL ,
                        Nom Varchar (50) NOT NULL
    ,CONSTRAINT couleur_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: candyTag
#------------------------------------------------------------

CREATE TABLE candyTag(
                         id  Int  Auto_increment  NOT NULL ,
                         nom Varchar (50) NOT NULL
    ,CONSTRAINT candyTag_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
                         id          Int  Auto_increment  NOT NULL ,
                         quantity    Int NOT NULL ,
                         id_client   Int NOT NULL ,
                         id_candyTag Int NOT NULL
    ,CONSTRAINT Commande_PK PRIMARY KEY (id)

    ,CONSTRAINT Commande_client_FK FOREIGN KEY (id_client) REFERENCES client(id)
    ,CONSTRAINT Commande_candyTag0_FK FOREIGN KEY (id_candyTag) REFERENCES candyTag(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: itemCandyBox
#------------------------------------------------------------

CREATE TABLE itemCandyBox(
                             id          Int NOT NULL ,
                             id_candyBox Int NOT NULL ,
                             quantity    Int NOT NULL
    ,CONSTRAINT itemCandyBox_PK PRIMARY KEY (id,id_candyBox)

    ,CONSTRAINT itemCandyBox_couleur_FK FOREIGN KEY (id) REFERENCES couleur(id)
    ,CONSTRAINT itemCandyBox_candyBox0_FK FOREIGN KEY (id_candyBox) REFERENCES candyBox(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: livraison
#------------------------------------------------------------

CREATE TABLE livraison(
                       id          Int NOT NULL ,
                       id_Commande Int NOT NULL
    ,CONSTRAINT livrer_PK PRIMARY KEY (id,id_Commande)

    ,CONSTRAINT livraison_candyBox_FK FOREIGN KEY (id) REFERENCES candyBox(id)
    ,CONSTRAINT livraison_Commande0_FK FOREIGN KEY (id_Commande) REFERENCES Commande(id)
)ENGINE=InnoDB;
