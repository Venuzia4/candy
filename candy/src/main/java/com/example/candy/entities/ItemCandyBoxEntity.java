package com.example.candy.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "itemCandyBox", schema = "candybd",uniqueConstraints = {
        @UniqueConstraint(columnNames = {"quantity"})
})
public class ItemCandyBoxEntity {

    @Id
    @ManyToOne
    @JoinColumn(name = "id", nullable=false)
    private CouleurEntity couleur;

    @Id
    @ManyToOne
    @JoinColumn(name = "id_candyBox" , nullable=false)
    private CandyBoxEntity candyBox;

    private int quantity;


}
