package com.example.candy.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "livraison", schema = "candybd")
public class LivraisonEntity {

    @Id
    @ManyToOne
    @JoinColumn(name = "id")
    private CandyBoxEntity candyBox;

    @Id
    @ManyToOne
    @JoinColumn(name = "id_Commande")
    private CommandeEntity commande;


}
