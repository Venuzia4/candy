package com.example.candy.repositories;

import com.example.candy.entities.CandyBoxEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICandyBoxRepository extends JpaRepository<CandyBoxEntity, Long> {
}
