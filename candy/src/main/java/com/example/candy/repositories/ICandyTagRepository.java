package com.example.candy.repositories;

import com.example.candy.entities.CandyTagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICandyTagRepository extends JpaRepository<CandyTagEntity,Long> {
}
