package com.example.candy.repositories;

import com.example.candy.entities.CommandeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICommandeRepository extends JpaRepository<CommandeEntity,Long> {
}
