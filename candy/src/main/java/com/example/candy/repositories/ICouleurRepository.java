package com.example.candy.repositories;

import com.example.candy.entities.CouleurEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICouleurRepository extends JpaRepository<CouleurEntity,Long> {
}
